package design

import . "goa.design/goa/http/design"
import . "goa.design/goa/http/dsl"

var _ = Service("child", func() {
	Description("The storage service makes it possible to view, add or remove wine bottles.")

	HTTP(func() {
		//Parent("parent")
		//Path("/child")
		Path("//parent/{parent_id}/child")
	})

	Method("list", func() {
		Description("List all stored bottles")
		Payload(func(){
			Attribute("parent_id", UInt64, "ID is the foreign key of the Parent.")
			Required("parent_id")
		})
		Result(CollectionOf(StoredBottle), func() {
			View("tiny")
		})
		HTTP(func() {
			GET("/")
			Response(StatusOK)
		})
	})

	Method("show", func() {
		Description("Show bottle by ID")
		Payload(func() {
			Attribute("parent_id", UInt64, "ID is the foreign key of the Parent.")
			Attribute("id", String, "ID of bottle to show")
			Attribute("view", String, "View to render", func() {
				Enum("default", "tiny")
			})
			Required("id", "parent_id")
		})
		Result(StoredBottle)
		Error("not_found", NotFound, "Bottle not found")
		HTTP(func() {
			GET("/{id}")
			Param("view")
			Response(StatusOK)
			Response("not_found", StatusNotFound)
		})
	})

	//Method("add", func() {
	//	Description("Add new bottle and return its ID.")
	//	Payload(Bottle)
	//	Result(String)
	//	HTTP(func() {
	//		POST("/")
	//		Response(StatusCreated)
	//	})
	//})

	Method("remove", func() {
		Description("Remove bottle from storage")
		Payload(func() {
			Attribute("parent_id", UInt64, "ID is the foreign key of the Parent.")
			Attribute("id", String, "ID of bottle to remove")
			Required("id", "parent_id")
		})
		Error("not_found", NotFound, "Bottle not found")
		HTTP(func() {
			DELETE("/{id}")
			Response(StatusNoContent)
		})
	})

	//Method("rate", func() {
	//	Description("Rate bottles by IDs")
	//	Payload(func() {
	//		Attribute("parent_id", UInt64, "ID is the foreign key of the Parent.")
	//		Required("parent_id")
	//	})
	//	Payload(MapOf(UInt32, ArrayOf(String)))
	//	HTTP(func() {
	//		POST("/rate")
	//		MapParams()
	//		Response(StatusOK)
	//	})
	//})
})
